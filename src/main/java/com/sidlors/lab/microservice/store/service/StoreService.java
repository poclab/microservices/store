package com.sidlors.lab.microservice.store.service;

import java.util.List;

import com.sidlors.lab.microservice.store.model.Order;



public interface StoreService {

	/**
	 * Finds order by given name
	 *
	 * @param orderId
	 * @return found order
	 */
	Order findById(Long  Id);

	/**
	 * Checks if order with the same name already exists
	 * Invokes Auth Service user creation
	 * Creates new order with default parameters
	 *
	 * @param order
	 * @return created order
	 */
	Order create(Order order);

	/**
	 * Validates and applies incoming order updates
	 * Invokes Statistics Service update
	 *
	 * @param name
	 * @param update
	 */
	void saveChanges(Order update);

	/**
	 * 
	 * @return
	 */
	List<Order> findAll();

	void deleteOrder(Order order);


}
