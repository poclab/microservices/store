package com.sidlors.lab.microservice.store.test.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sidlors.lab.microservice.store.model.Order;
import com.sidlors.lab.microservice.store.repository.OrderRepository;
import com.sidlors.lab.microservice.store.service.StoreServiceImpl;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import org.springframework.transaction.annotation.Propagation;

@RunWith(SpringRunner.class)
public class OrderServiceTest {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@InjectMocks
	private StoreServiceImpl storeService;


	@MockBean(name = "orderRepository")
	private OrderRepository orderRepository;

	@Before
	public void setup() {
		initMocks(this);
	}

	
	@Test
	@DisplayName("Should get tostring object Order")
	public void shoulGetToStingObject(){
		Order order = new Order();
		order.setId(11L);
		order.setPetId(12L);
		assertEquals(order.toString().toCharArray().length,115);
	}

	@Test
	@DisplayName("Should is equals object")
	public void shouldIsEqualObject(){
		Order order = new Order();
		order.setId(11L);
		assertEquals(order.equals(order),true);
	}


	@Test
	@DisplayName("Should is not equals object")
	public void shouldIsNotEqualObject(){
		Order order = new Order();
		Order order2 = new Order();
		order.setId(11L);
		assertEquals(order.equals(order2),false);
	}

	@Test
	@DisplayName("Should get hastcode order")
	public void shouldGetHastcodeOrder(){
		Order order = new Order();
		order.setId(11L);
		order.setPetId(12L);
		assertEquals(order.hashCode(),1213507831);
	}


	@Test
	@DisplayName("Should create order with given user")
	public void shouldCreateOrderWithGivenUser() {
		Order order = new Order();
		order.setId(11L);
		order.setPetId(12L);
		order.setComplete(true);
		Order resOrder=storeService.create(order);
		assertEquals(resOrder.getPetId(),order.getPetId());
		assertEquals(resOrder.getId() ,order.getId());
		assertEquals(resOrder.isComplete() ,true);

		log.info("asotmm "+resOrder.complete(true));

		verify(orderRepository, times(1)).save(order);
	}

	@Test
	@DisplayName("Should delete order with given user")
	public void shouldDeleteOrderWithGivenUser(){
		Order order = new Order();
		Order resOrder =storeService.create(order);
		storeService.deleteOrder(resOrder);
		assertEquals(resOrder.getPetId(), null);
	}


	@Test
	@DisplayName("Should get all orders null")
	public void shouldGetallOrdersNull(){
		List<Order> stores = storeService.findAll();
		assertEquals(stores, null);
	}

	@Test
	@DisplayName("Should get all orders")
	public void shouldGetAllOrders(){
		createOrderbyId();
		List<Order> stores = storeService.findAll();
        List<String> actual = Arrays.asList("a", "b", "c");
		assertEquals(actual.size(),3);

	}

	private Order createOrderbyId() {		
		Order order = new Order();
		order.setId(11L);		
		return  storeService.create(order);
	}
}
