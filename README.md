# store
[![Docker Repository on Quay](https://quay.io/repository/sidlors/store/status?token=ab7b0563-751f-4d7c-b890-51ce78f5e0c8 "Docker Repository on Quay")](https://quay.io/repository/sidlors/store)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-store&metric=alert_status)](https://sonarcloud.io/dashboard?id=sidlors-lab-store)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-store&metric=code_smells)](https://sonarcloud.io/dashboard?id=sidlors-lab-store)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-store&metric=coverage)](https://sonarcloud.io/dashboard?id=sidlors-lab-store)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-store&metric=ncloc)](https://sonarcloud.io/dashboard?id=sidlors-lab-store)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-store&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=sidlors-lab-store)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-store&metric=security_rating)](https://sonarcloud.io/dashboard?id=sidlors-lab-store)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-store&metric=sqale_index)](https://sonarcloud.io/dashboard?id=sidlors-lab-store)